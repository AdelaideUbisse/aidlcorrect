package com.codekul.aidlclient;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;

import com.codekul.aidlserver.IComman;

public class MainActivity extends AppCompatActivity {

    IComman comman;
    ServiceConnection serviceConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                Log.i("TAG", "onServiceConnected");
                comman = IComman.Stub.asInterface(iBinder);
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {
                Log.i("TAG", "onServiceConnected");
                comman = null;
            }
        };
        findViewById(R.id.btnBind).setOnClickListener(view -> {
            Intent intent = new Intent("com.codekul.aidlserver.AIDL");
            intent.setPackage("com.codekul.aidlserver");
            bindService(intent,serviceConnection,BIND_AUTO_CREATE);
        });

        findViewById(R.id.btnCalculate).setOnClickListener(view -> {
            try {
                Log.i("@codekul","Addition is -"+comman.calculate(10,100));
            } catch (NullPointerException | RemoteException e) {
                e.printStackTrace();
            }
        });
    }
}