// IComman.aidl
package com.codekul.aidlserver;

// Declare any non-default types here with import statements

interface IComman {
  int calculate(int num1, int num2);
}