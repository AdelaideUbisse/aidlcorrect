package com.codekul.aidlserver;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;

public class MyService extends Service {
    MyImpl impl =  new MyImpl();
    public MyService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return impl;

    }
    public class MyImpl extends IComman.Stub{

        @Override
        public int calculate(int num1, int num2) throws RemoteException {
            return (num1+num2);
        }
    }
}